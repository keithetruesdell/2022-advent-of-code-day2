package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

const (
	example1File string = "input/example1.txt"
	prodFile     string = "input/prodFile.txt"
)

func main() {
	var (
		err error
	)

	iptF := getFlags()
	fileName := getFileName(*iptF)
	dataArr, err := getFileData(fileName)
	if err != nil {
		fmt.Printf("--- ERROR ---\n    %v", err)
		return
	}

	// testing and dev output
	if *iptF > 0 {
		fmt.Printf("dataArr: %v\n", dataArr)
	}

	ttlScore, err := getTotalScore(dataArr)
	if err != nil {
		fmt.Printf("--- ERROR ---\n     %v", err)
		return
	}

	fmt.Println("=============")

	fmt.Printf("TOTAL SCORE: %v\n", ttlScore)
}

func getTotalScore(data [][]string) (score int, err error) {
	//fmt.Println("-------")
	for _, round := range data {
		res := roundWinLoseDraw(round)
		rndSc := roundScore(res, round[1]) // just needed to flip these??
		//fmt.Printf("Elf: %v   Me: %v   Res: %v   CalcSc: %v\n", round[0], res, round[1], rndSc)
		score = score + rndSc
	}

	return score, nil
}

// ELF HAND
// A = Rock
// B = Paper
// C = Scissors

// MY HAND
// X = Lose   ~Rock~     = 1
// Y = Draw   ~Paper~    = 2
// Z = Win    ~Scissors~ = 3
func roundWinLoseDraw(round []string) (roundResult string) {
	elfHand := round[0]
	myHand := round[1]

	switch elfHand {
	case "A": // Rock
		switch myHand {
		case "X":
			return "C" // Rock Beats Scissors - Lose // Original ~"D"~
		case "Y":
			return "A" // Rock Ties Rock - Draw // Original ~"W"~
		case "Z":
			return "B" // Rock loses Paper - Win // Original ~"L"~
		}
	case "B": // Paper
		switch myHand {
		case "X":
			return "A" // Paper beats Rock - Lose // Original ~"L"~
		case "Y":
			return "B" // Paper ties Paper - Draw // Original ~"D"~
		case "Z":
			return "C" // Paper loses to Scissors - Win // Original ~"W"~
		}
	case "C": // Scissors
		switch myHand {
		case "X":
			return "B" // Scissors beats Paper - Lose // Original ~"W"~
		case "Y":
			return "C" // Scissors ties Scissors - Draw // Original ~"L"~
		case "Z":
			return "A" // Scissors loses to Rock - Win // Original ~"D"~
		}
	}

	return roundResult
}

// X = Rock     = 1
// Y = Paper    = 2
// Z = Scissors = 3
func roundScore(hand string, outcome string) (sc int) {
	switch hand {
	case "A":
		sc = sc + 1
	case "B":
		sc = sc + 2
	case "C":
		sc = sc + 3
	}

	switch outcome {
	case "Z": // original part1 ~"W"~:
		sc = sc + 6
	case "X": // original part1 ~"L"~:
		sc = sc + 0
	case "Y": // original part1 ~"D"~:
		sc = sc + 3
	}
	return sc
}

func getFlags() (f *int) {
	f = flag.Int("e", 1, "Which file to use?  1 is default for the first example, and 0 is prod")
	flag.Parse()
	return f
}

func getFileName(i int) string {
	switch i {
	case 0:
		return prodFile
	case 1:
		return example1File
	default:
		return example1File
	}
}

func getFileData(fStr string) (arrStr [][]string, err error) {
	fs, err := os.Open(fStr)
	if err != nil {
		return nil, err
	}
	defer fs.Close()

	scanner := bufio.NewScanner(fs)
	for scanner.Scan() {
		tmpStr := scanner.Text()
		tmpArr := strings.Fields(strings.TrimSpace(tmpStr))
		arrStr = append(arrStr, tmpArr)
	}

	err = fs.Close()
	if err != nil {
		return nil, err
	}

	return arrStr, nil
}
